## IMPORTING STUFF

from tkinter import *
import matplotlib.pyplot as plt

## GUI FUNCTIONS

def continueClick():
    global heightV, widthV
    heightV, widthV = heightEntry.get(), widthEntry.get()
    for x in root.winfo_children():
        x.destroy()
    buildGrid()
    buildEntry()

def submitClick():
    allEntries,xyList = [],[]
    for i in range(0,int(heightV)):
        allEntries += []
        for j in range(0, int(widthV)):
            ijList += [[i,j]]

def buildEntry():
    global entries
    entries = []
    xyList = []
    for x in range(0,int(heightV)):
        entries += []
        for y in range(0,int(widthV)):
            entries += [Entry(root,width=3)]
            xyList += [[x,y]]
    for i in range(len(entries)):
        entries[i].grid(row = xyList[i][0], column = xyList[i][1])

def buildGrid():
    global grid
    grid = []
    for x in range(0,int(heightV)):
        grid += [[]]
        for y in range(0,int(widthV)):
            grid[x] += [0]

## TKINTER INTERFACE

root = Tk()
root.title("Starting Window")

def inputWindow():
    global heightEntry,widthEntry
    instruct = Label(root, text='Type in dimensions of puzzle.')
    instruct.grid(row=0, column=0, columnspan=2)

    heightLabel = Label(root, text='Height:')
    heightLabel.grid(row=1, column=0, sticky=E)

    heightEntry = Entry(root, width=10)
    heightEntry.grid(row=1, column=1)

    widthLabel = Label(root, text='Row:')
    widthLabel.grid(row=2,column=0, sticky=E)

    widthEntry = Entry(root, width=10)
    widthEntry.grid(row=2, column=1)

    exitButton = Button(root, text='Exit', width=8)
    exitButton.grid(row=3, column=0)

    continueButton = Button(root, text='Continue', width=8, command=continueClick)
    continueButton.grid(row=3, column=1)

inputWindow()

root.mainloop()
