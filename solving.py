import copy
from copy import deepcopy
from random import randint

def checkHashi(hashi): ## This ONLY checks if it is a rectangle, not if it is solvable.
    n = len(hashi[0])
    for eachRow in hashi:
        if len(eachRow) != n:
            return False
    return True

bascHashi = [[0,2,0,0,2,0,1,0],
             [1,0,0,2,0,3,0,2],
             [0,2,0,0,0,0,0,0],
             [3,0,0,0,0,2,0,3],
             [0,4,0,4,0,0,2,0],
             [3,0,0,0,1,0,0,2],
             [0,4,0,3,0,0,1,0],
             [2,0,2,0,3,0,0,2]]

bridHashi = [[2,3,0,4,0,2,0],
             [0,0,0,0,0,0,2],
             [1,1,0,0,1,3,3],
             [2,0,0,8,0,5,2],
             [3,0,3,'d',0,0,1],
             [0,0,2,'d',0,3,4],
             [3,0,0,3,1,0,2]]

easyHashi = [[2,0,0,1,0,0,1,0],
             [0,0,0,0,0,0,0,0],
             [0,1,0,6,0,0,4,0],
             [0,0,0,0,0,0,0,0],
             [5,0,0,5,0,1,0,0],
             [0,2,0,0,0,0,3,0],
             [2,0,1,0,0,0,0,0],
             [0,0,0,0,0,0,0,0]]

easyHashiT = [[2,0,0,0,5,0,2],
              [0,0,1,0,0,2,0],
              [0,0,0,0,0,0,1],
              [1,0,6,0,5,0,0],
              [0,0,0,0,0,0,0],
              [0,0,0,0,1,0,0]]

easyHashi1 = [[2,0,2,0,3,0,0,2,0,0],
              [0,3,0,0,0,2,0,0,0,3],
              [0,0,0,3,0,0,3,0,2,0],
              [3,0,0,0,0,0,0,0,0,3],
              [0,5,0,4,0,4,0,3,0,0],
              [4,0,0,0,0,0,0,0,3,0],
              [0,3,0,0,1,0,0,2,0,2],
              [3,0,2,0,0,7,0,0,5,0],
              [0,3,0,0,2,0,0,0,0,1],
              [3,0,0,3,0,2,0,0,1,0]]

mediHashi1 = [[2,0,0,2,0,1,0],
              [0,0,1,0,3,0,1],
              [3,0,0,2,0,0,0],
              [0,0,0,0,3,0,2],
              [0,1,0,2,0,0,0],
              [2,0,2,0,2,0,0],
              [0,1,0,3,0,0,3]]

def countRows(hashi):
    return len(hashi)

def countCols(hashi):
    return len(hashi[0])

def bridgeLimit(hashi):
    bLimit = 0
    for i in range(countRows(hashi)):
        for j in range(countCols(hashi)):
            if isinstance(hashi[i][j],int) == True:
                bLimit += hashi[i][j]
            else:
                pass
    return bLimit // 2 ## Graph theory

def coordsList(hashi): ## This gives a list of lists, each list is [amount of possible bridges, y coordinate, x coordinate]
    nodeList = []
    for i in range(countRows(hashi)):
        for j in range(countCols(hashi)):
            if findValue(hashi,i,j) != 0 and isinstance(findValue(hashi,i,j),int) == True:
                nodeList += [[hashi[i][j],i,j]]
            elif findValue(hashi,i,j) == 'x':
                nodeList += [[hashi[i][j],i,j]]
    return nodeList

def findValue(hashi,x,y):
    return hashi[x][y]

def checkObstruction(hashi,xO,yO,xT,yT):
    if xO == xT:
        for i in range(abs(yT-yO)-1):
            if isinstance(hashi[xO][min([yO,yT])+i+1],str) == True and hashi[xO][min([yO,yT])+i+1] != 'a':
                return True
            else:
                pass
        return False
    elif yO == yT:
        for i in range(abs(xT-xO)-1):
            if isinstance(hashi[min([xO,xT])+i+1][yO],str) == True and hashi[min([xO,xT])+i+1][yO] != 'c':
                return True
            else:
                pass
        return False

def checkObstructionX(hashi,xO,yO,xT,yT):
    if xO == xT:
        for i in range(abs(yT-yO)-1):
            if isinstance(hashi[xO][min([yO,yT])+i+1],str) == True and hashi[xO][min([yO,yT])+i+1] not in ['a','b','x']:
                return True
            else:
                pass
        return False
    elif yO == yT:
        for i in range(abs(xT-xO)-1):
            if isinstance(hashi[min([xO,xT])+i+1][yO],str) == True and hashi[min([xO,xT])+i+1][yO] not in ['c','d','x']:
                return True
            else:
                pass
        return False

def sameRow(hashi,x,y):
    coordRListP = []
    if findValue(hashi,x,y) == 0:
        return
    else:
        for eachCoord in coordsList(hashi):
            if eachCoord[1] == x:
                if eachCoord[2] != y and isinstance(eachCoord[0],int) == True:
                    coordRListP += [eachCoord]
                else:
                    pass
            else:
                pass
    coordRList = [coords for coords in coordRListP if checkObstruction(hashi,x,y,coords[1],coords[2]) == False]
    return coordRList

def sameCol(hashi,x,y):
    coordCListP = []
    if findValue(hashi,x,y) == 0:
        return
    else:
        for eachCoord in coordsList(hashi):
            if eachCoord[2] == y:
                if eachCoord[1] != x and isinstance(eachCoord[0],int) == True:
                    coordCListP += [eachCoord]
                else:
                    pass
            else:
                pass
    coordCList = [coords for coords in coordCListP if checkObstruction(hashi,x,y,coords[1],coords[2]) == False]
    return coordCList

def sameRowX(hashi,x,y):
    coordRListP = []
    if findValue(hashi,x,y) == 0:
        return
    else:
        for eachCoord in coordsList(hashi):
            if eachCoord[1] == x:
                if eachCoord[2] != y and isinstance(eachCoord[0],int) == True:
                    coordRListP += [eachCoord]
                elif eachCoord[2] != y and eachCoord[0] == 'x':
                    coordRListP += [eachCoord]
                else:
                    pass
            else:
                pass
    coordRList = [coords for coords in coordRListP if checkObstructionX(hashi,x,y,coords[1],coords[2]) == False]
    return coordRList

def sameColX(hashi,x,y):
    coordCListP = []
    if findValue(hashi,x,y) == 0:
        return
    else:
        for eachCoord in coordsList(hashi):
            if eachCoord[2] == y:
                if eachCoord[1] != x and isinstance(eachCoord[0],int) == True:
                    coordCListP += [eachCoord]
                elif eachCoord[1] != x and eachCoord[0] == 'x':
                    coordCListP += [eachCoord]
                else:
                    pass
            else:
                pass
    coordCList = [coords for coords in coordCListP if checkObstructionX(hashi,x,y,coords[1],coords[2]) == False]
    return coordCList

def adjacentRow(hashi,x,y):
    colCoordL = [eachCoord[2] for eachCoord in sameRow(hashi,x,y)]
    lowerRow = [eachCoord for eachCoord in colCoordL if eachCoord < y]
    higherRow = [eachCoord for eachCoord in colCoordL if eachCoord > y]
    if len(lowerRow) == 0:
        if len(higherRow) == 0:
            return []
        else:
            lowestHigh = min(higherRow)
            return [[findValue(hashi,x,lowestHigh),x,lowestHigh]]
    else:
        highestLow = max(lowerRow)
        if len(higherRow) == 0:
            return [[findValue(hashi,x,highestLow),x,highestLow]]
        else:
            lowestHigh = min(higherRow)
            return [[findValue(hashi,x,highestLow),x,highestLow], [findValue(hashi,x,lowestHigh),x,lowestHigh]]

def adjacentCol(hashi,x,y):
    rowCoordL = [eachCoord[1] for eachCoord in sameCol(hashi,x,y)]
    lowerCol = [eachCoord for eachCoord in rowCoordL if eachCoord < x]
    higherCol = [eachCoord for eachCoord in rowCoordL if eachCoord > x]
    if len(lowerCol) == 0:
        if len(higherCol) == 0:
            return []
        else:
            lowestHigh = min(higherCol)
            return [[findValue(hashi,lowestHigh,y),lowestHigh,y]]
    else:
        highestLow = max(lowerCol)
        if len(higherCol) == 0:
            return [[findValue(hashi,highestLow,y),highestLow,y]]
        else:
            lowestHigh = min(higherCol)
            return [[findValue(hashi,highestLow,y),highestLow,y],[findValue(hashi,lowestHigh,y),lowestHigh,y]]

def adjacentRowX(hashi,x,y):
    colCoordL = [eachCoord[2] for eachCoord in sameRowX(hashi,x,y)]
    lowerRow = [eachCoord for eachCoord in colCoordL if eachCoord < y]
    higherRow = [eachCoord for eachCoord in colCoordL if eachCoord > y]
    if len(lowerRow) == 0:
        if len(higherRow) == 0:
            return []
        else:
            lowestHigh = min(higherRow)
            return [[findValue(hashi,x,lowestHigh),x,lowestHigh]]
    else:
        highestLow = max(lowerRow)
        if len(higherRow) == 0:
            return [[findValue(hashi,x,highestLow),x,highestLow]]
        else:
            lowestHigh = min(higherRow)
            return [[findValue(hashi,x,highestLow),x,highestLow], [findValue(hashi,x,lowestHigh),x,lowestHigh]]

def adjacentColX(hashi,x,y):
    rowCoordL = [eachCoord[1] for eachCoord in sameColX(hashi,x,y)]
    lowerCol = [eachCoord for eachCoord in rowCoordL if eachCoord < x]
    higherCol = [eachCoord for eachCoord in rowCoordL if eachCoord > x]
    if len(lowerCol) == 0:
        if len(higherCol) == 0:
            return []
        else:
            lowestHigh = min(higherCol)
            return [[findValue(hashi,lowestHigh,y),lowestHigh,y]]
    else:
        highestLow = max(lowerCol)
        if len(higherCol) == 0:
            return [[findValue(hashi,highestLow,y),highestLow,y]]
        else:
            lowestHigh = min(higherCol)
            return [[findValue(hashi,highestLow,y),highestLow,y],[findValue(hashi,lowestHigh,y),lowestHigh,y]]

def allAdjacent(hashi,x,y):
    return adjacentCol(hashi,x,y) + adjacentRow(hashi,x,y)

def allAdjacentX(hashi,x,y):
    return adjacentColX(hashi,x,y) + adjacentRowX(hashi,x,y)

def minusFromNode(hashi,xO,yO,xT,yT,i):
    hashiO = hashi.copy()
    if isinstance(hashi[xO][yO],int) == True and isinstance(hashi[xT][yT],int) == True: 
        if hashi[xO][yO] >= i and hashi[xT][yT] >= i:
            hashiO[xO][yO] -= i
            hashiO[xT][yT] -= i
            if hashiO[xO][yO] == 0:
                hashiO[xO][yO] = 'x'
            if hashiO[xT][yT] == 0:
                hashiO[xT][yT] = 'x' ### LOOK AT THIS AFTERWARDS, SHOULD NOT MAKE BOTH ENDS ISOLATED FOR A CORRECT SOLUTION
            else:
                pass
        else:
            raise ValueError('Bridge can not be built here.')
    else:
        raise ValueError('Non valid bridge end points')
    return hashiO

def makeDoubleBridge(hashi,xO,yO,xT,yT):
    if xO == xT: ## case that x coordinates are equal
        minusFromNode(hashi,xO,yO,xT,yT,2)
        for i in range(abs(yT-yO)-1):
            hashi[xO][min([yO,yT])+i+1] = 'b'
    elif yO == yT: ## case that y coordinates are equal
        minusFromNode(hashi,xO,yO,xT,yT,2)
        for i in range(abs(xT-xO)-1):
            hashi[min([xO,xT])+i+1][yO] = 'd'
    else:
        raise ValueError('Coordinates must share either x or y values.')
    return hashi

def makeSingleBridge(hashi,xO,yO,xT,yT):
    if xO == xT: ## case that x coordinates are equal
        minusFromNode(hashi,xO,yO,xT,yT,1)
        for i in range(abs(yT-yO)-1):
            hashi[xO][min([yO,yT])+i+1] = 'a'
    elif yO == yT: ## case that y coordinates are equal
        minusFromNode(hashi,xO,yO,xT,yT,1)
        for i in range(abs(xT-xO)-1):
            hashi[min([xO,xT])+i+1][yO] = 'c'
    else:
        raise ValueError('Coordinates must share either x or y values.')
    return hashi

def addBridge(hashi,xO,yO,xT,yT,bridgeList=None): # include a checker to make sure it doesn't isolate a component
    print('adding 1 between (' + str(xO) + ', ' + str(yO) + ') and (' + str(xT) + ', ' + str(yT) + ')')
    if bridgeList == None:
        bridgeList = []
    if xO == xT: ## case that x coordinates are equal
        hashiC = hashi.copy()
        bridgeList += [[[xO,yO],[xT,yT]]]
        hashiO = minusFromNode(hashiC,xO,yO,xT,yT,1)
        for i in range(abs(yT-yO)-1):
            if hashiO[xO][min([yO,yT])+i+1] == 'a':
                hashiO[xO][min([yO,yT])+i+1] = 'b'
            else:
                hashiO[xO][min([yO,yT])+i+1] = 'a'
    elif yO == yT: ## case that y coordinates are equal
        hashiC = hashi.copy()
        bridgeList += [[[xO,yO],[xT,yT]]]
        hashiO = minusFromNode(hashiC,xO,yO,xT,yT,1)
        for i in range(abs(xT-xO)-1):
            if hashiO[min([xO,xT])+i+1][yO] == 'c':
                hashiO[min([xO,xT])+i+1][yO] = 'd'
            else:
                hashiO[min([xO,xT])+i+1][yO] = 'c'
    else:
        raise ValueError('Coordinates must share either x or y values.')
    print(hashi)
    return hashiO

def makeAllStrings(hashi):
    hashiO = [[[] for j in range(len(hashi[0]))] for i in range(len(hashi))]
    for i in range(len(hashi)):
        for j in range(len(hashi[0])):
            hashiO[i][j] = str(hashi[i][j])
    return hashiO

nodesBridges = [[8,4],[7,4],[6,3],[5,3],[4,2],[3,2],[2,1],[1,1]]

def adjValue(hashi,x,y,k):
    return [node for node in allAdjacent(hashi,x,y) if node[0]==k]

def reverseAdjValue(hashi,x,y,k):
    return [node for node in allAdjacent(hashi,x,y) if node[0]!=k]

def countAdjValue(hashi,x,y,k):
    return len(adjValue(hashi,x,y,k))

### MAKE A FUNCTION TO CHECK ALL THE CONNECTED PARTS OF HASHI AND SEE IF IT IS THE WHOLE THING ??

def hasBridge(hashi,xO,yO,xT,yT):
    if xO != xT and yO != yT:
        return False # As there cannot be any bridges if they do not share a row or a column.
    elif xO == xT:
        for i in range(abs(yT-yO)-1):
            if hashi[xO][min([yO,yT])+i+1] == 'a' or hashi[xO][min([yO,yT])+i+1] == 'b':
                pass
            else:
                return False
            return True
    elif yO == yT:
        for i in range(abs(xT-xO)-1):
            if hashi[min([xO,xT])+i+1][yO] == 'c' or hashi[min([xO,xT])+i+1][yO] == 'd':
                pass
            else:
                return False
            return True
    return

def findConnectedComponent(hashi,x,y,connected=None):
    if connected == None:
        connected = []
        comSize = len(connected)
    if [hashi[x][y],x,y] in connected:
        return False
    connected += [[hashi[x][y],x,y]]
    for node in [node for node in allAdjacentX(hashi,x,y) if node not in connected if hasBridge(hashi,x,y,node[1],node[2]) == True]:
        findConnectedComponent(hashi,node[1],node[2],connected)
    return connected

def isComponentIsolated(hashi,x,y):
    connectedComponents = findConnectedComponent(hashi,x,y)
    for eachComp in connectedComponents:
        if isinstance(eachComp[0],int) == True:
            return False
    return True

def makeClone(hashi):
    return [hashi[i][:] for i in range(len(hashi))]

print(makeClone(bascHashi))

def originalValue(hashi,x,y):
    c = hashi[x][y]
    if x != 0:
        if hashi[x-1][y] == 'c':
            c += 1
        elif hashi[x-1][y] == 'd':
            c += 2
    if x != countRows(hashi)-1:
        if hashi[x+1][y] == 'c':
            c += 1
        elif hashi[x+1][y] == 'd':
            c += 2
    if y != 0:
        if hashi[x][y-1] == 'a':
            c += 1
        elif hashi[x][y-1] == 'b':
            c += 2
    if y != countCols(hashi)-1:
        if hashi[x][y+1] == 'a':
            c += 1
        elif hashi[x][y+1] == 'b':
            c += 2
    return c

def solveHashi(hashi,bridgeList=None,nodeList=None,l=None):
    print('new loop')
    if bridgeList == None:
        bridgeList = []
    if l == None:
        l = bridgeLimit(hashi)
    if nodeList == None:
        nodeList = coordsList(hashi)
    if len(bridgeList) == l:
        return hashi
    for i in range(len(nodeList)):
        allAdj = allAdjacent(hashi,nodeList[i][1],nodeList[i][2])
        nodeV = hashi[nodeList[i][1]][nodeList[i][2]]
        if nodeV == 4 and len(allAdj) == 3: ## CASE WHEN A NODE HAS 4, 3 POSSIBLE BRIDGES AND 2 ARE 1.
            #print(nodeList[i],allAdj)
            if countAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) == 2:
                #print('1 2')
                for eachAdjV in adjValue(hashi,nodeList[i][1],nodeList[i][2],1):
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
                for eachAdjV in adjValue(hashi,nodeList[i][1],nodeList[i][2],2): # This is the remaining node, as 4 - (1 + 1) = 2, and there are 3 adjacent nodes, hence it must be a 2.
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
            elif countAdjValue(hashi,nodeList[i][1],nodeList[i][2],2) == 2: # Case when there are three adjacent nodes to a 4 node, 2 of the adjacent have capacity 2. The node without capacity of 2 must have at least 1 bridge, else it will create an isolated component.
                originalTwos = True
                for eachAdjV in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],2):
                    if originalValue(hashi,eachAdjV[1],eachAdjV[2]) != 2:
                        originalTwos = False
                if originalTwos == True:
                    for eachAdjV in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],2):
                        hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                        solveHashi(hashi,bridgeList,nodeList,l)
        elif nodeV == 6 and len(allAdj) == 4:
            #print(nodeList[i],allAdj)
            if countAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) == 1:
                #print('1 1 6 4')
                for eachAdjV in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],1):
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
            elif countAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) == 2:
                #print('1 2')
                for eachAdjV in adjValue(hashi,nodeList[i][1],nodeList[i][2],1):
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
                for eachAdjV in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],1):
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
        elif nodeV == 3 and len(allAdj) == 3:
            if originalValue(hashi,nodeList[i][1],nodeList[i][2]) == 3: # make diagram to show this is to prevent isolation, if it has another bridge connected then it's not necessarily isolated.
                if countAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) == 1:
                    if countAdjValue(hashi,nodeList[i][1],nodeList[i][2],2) == 2:
                        for remainingNode in [node for node in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) if node in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],2)]: # this will be just one node
                            hashi = addBridge(hashi,remainingNode[1],remainingNode[2],nodeList[i][1],nodeList[i][2])
                            solveHashi(hashi,bridgeList,nodeList,l)
        if isinstance(nodeV,int) == True:
            if nodeV > 1 and len(allAdj) == 2:
                #print(nodeList[i],allAdj)
                if countAdjValue(hashi,nodeList[i][1],nodeList[i][2],1) == 1:
                    #print('1 1 2 2')
                    for eachAdjV in reverseAdjValue(hashi,nodeList[i][1],nodeList[i][2],1):
                        hashi = addBridge(hashi,eachAdjV[1],eachAdjV[2],nodeList[i][1],nodeList[i][2],bridgeList)
                        solveHashi(hashi,bridgeList,nodeList,l)
    for eachPair in nodesBridges: ## GENERAL CASES leave these
        for i in range(len(nodeList)):
            allAdj = allAdjacent(hashi,nodeList[i][1],nodeList[i][2])
            if hashi[nodeList[i][1]][nodeList[i][2]] == eachPair[0] and len(allAdj) == eachPair[1]: ## If nodeList[i][0] == 8, then there must be four adjacent nodes, which each must have a double bridge connecting each node with the '8 node'.
                print('eachPair')
                print(eachPair)
                for j in range(len(allAdj)):
                    if eachPair[0] % 2 == 0:
                        hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[j][1],allAdj[j][2],bridgeList)
                        hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[j][1],allAdj[j][2],bridgeList)
                    else:
                        hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[j][1],allAdj[j][2],bridgeList)
                solveHashi(hashi,bridgeList,nodeList,l)
    for i in range(len(nodeList)):
        if isinstance(hashi[nodeList[i][1]][nodeList[i][2]],int) == True:
            allAdj = allAdjacent(hashi,nodeList[i][1],nodeList[i][2])
            nodeV = hashi[nodeList[i][1]][nodeList[i][2]]
            if len(allAdj) == 1: ## if a node is only adjacent to one node
                print('adj to just one')
                print(nodeList[i],allAdj)
                hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[0][1],allAdj[0][2],bridgeList)
                solveHashi(hashi,bridgeList,nodeList,l)
            if len(allAdj) == 2:
                print("CHECKING IF ISOLATED")
                print(nodeList[i],allAdj)
#                print(hashi)
                hashiA = addBridge(makeClone(hashi),nodeList[i][1],nodeList[i][2],allAdj[0][1],allAdj[0][2])
                hashiB = addBridge(makeClone(hashi),nodeList[i][1],nodeList[i][2],allAdj[1][1],allAdj[1][2])
#                print(hashi)
                if isComponentIsolated(hashiA,nodeList[i][1],nodeList[i][2]) == True:
                    print(findConnectedComponent(hashiA,nodeList[i][1],nodeList[i][2]))
                    print("added one a")
                    print(hashi[nodeList[i][1]][nodeList[i][2]],nodeList[i][1],nodeList[i][2])
                    hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[1][1],allAdj[1][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
                elif isComponentIsolated(hashiB,nodeList[i][1],nodeList[i][2]) == True:
                    print("added one b")
                    hashi = addBridge(hashi,nodeList[i][1],nodeList[i][2],allAdj[0][1],allAdj[0][2],bridgeList)
                    solveHashi(hashi,bridgeList,nodeList,l)
                else:
                    print("none added")
                    pass
                print("CHECKED IF ISOLATED")
    if len(bridgeList) == l:
        return hashi, bridgeList
    return hashi

def allPossibleBridges(hashi):
    possibleList = []
    for node in coordsList(hashi):
        if isinstance(node[0],int) == True:
            for otherNode in [i for i in coordsList(hashi) if i in allAdjacent(hashi,node[1],node[2])]:
                if isinstance(otherNode[0],int) == True:
                    if node[1] > otherNode[1]:
                        if node[0] >=2 and otherNode[0] >= 2:
                            possibleList += [[2,[node[1],node[2]],[otherNode[1],otherNode[2]]]]
                        elif node[0] >=1 and otherNode[0] >= 1:
                            possibleList += [[1,[node[1],node[2]],[otherNode[1],otherNode[2]]]]
                    if node[1] == otherNode[1]:
                        if node[2] > otherNode[2]:
                            if node[0] >=2 and otherNode[0] >= 2:
                                possibleList += [[2,[node[1],node[2]],[otherNode[1],otherNode[2]]]]
                            elif node[0] >=1 and otherNode[0] >= 1:
                                possibleList += [[1,[node[1],node[2]],[otherNode[1],otherNode[2]]]]
    return possibleList

def bruteForce(hashi,l=None):
    print('new loop')
    hashiO = makeClone(hashi)
    bridgeList = []
    if l == None:
        l = bridgeLimit(hashi)
    if len(bridgeList) == l:
        return hashiO
    while len(bridgeList) < l:
        possLength = len(allPossibleBridges(hashiO))
        if possLength < l - len(bridgeList): # If there are less possible bridges than there are bridges that need to be added, try again.
            bruteForce(hashi)
        print(possLength)
        if possLength == 0:
            addBridge(hashiO,allPossibleBridges(hashiO)[1][0],allPossibleBridges(hashiO)[1][1],allPossibleBridges(hashiO)[2][0],allPossibleBridges(hashiO)[2][1])
            bridgeList += [[[allPossibleBridges(hashiO)[0][1][0],allPossibleBridges(hashiO)[0][1][1]],[allPossibleBridges(hashiO)[0][2][0],allPossibleBridges(hashiO)[0][2][1]]]]
        else:
            randomBridgeNodes = allPossibleBridges(hashiO)[randint(0,possLength-1)]
            addBridge(hashiO,randomBridgeNodes[1][0],randomBridgeNodes[1][1],randomBridgeNodes[2][0],randomBridgeNodes[2][1])
            bridgeList += [[[randomBridgeNodes[1][0],randomBridgeNodes[1][1]],[randomBridgeNodes[2][0],randomBridgeNodes[2][1]]]]
        print(len(bridgeList),l)
        print(len(bridgeList),l)
        print(len(bridgeList),l)
        print(len(bridgeList),l)
    if len(findConnectedComponent(hashiO,coordsList(hashiO)[0][1],coordsList(hashiO)[0][2])) == len(coordsList(hashiO)):
        print('correct')
        return hashiO
    return hashiO

#print(makeAllStrings([[0, 2, 0, 0, 2, 0, 1, 0], [1, 0, 0, 2, 0, 3, 0, 2], [0, 2, 0, 0, 0, 0, 0, 0], [2, 0, 0, 0, 0, 2, 0, 3], ['c', 2, 0, 4, 0, 0, 2, 0], [1, 'd', 0, 0, 1, 0, 0, 2], ['c', 'x', 'b', 1, 0, 0, 1, 0], [1, 0, 2, 0, 3, 0, 0, 2]]))

#print(adjValue(bascHashi,4,1,4))
#print(countAdjValue(bascHashi,4,1,4))

#print(bridgeLimit(easyHashiT))

#print(easyHashi[5])
#solved = [['x', 0, 0, 'x', 0, 0, 'x', 0], ['d', 0, 0, 'c', 0, 0, 'c', 0], ['d', 'x', 'a', 'x', 'b', 'b', 'x', 0], ['d', 0, 0, 'd', 0, 0, 'c', 0], ['x', 'b', 'b', 'x', 'a', 'x', 'c', 0], ['c', 'x', 'b', 'b', 'b', 'b', 'x', 0], [1, 0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]

#print(bridgeLimit(mediHashi1))
#print(solveHashi(mediHashi1))

attemptSolve = solveHashi(bascHashi)

print(bruteForce(attemptSolve))

#print(bruteForce([[2,0,2]]))

#print(allPossibleBridges(bascHashi))

#print(bridgeLimit(bascHashi))
#print(bridgeLimit(bascHashi))
#print(bascHashi)
#print(minusFromNode(bascHashi,0,1,2))

#print(makeAllStrings(addBridge(bascHashi,0,1,0,4)))
#print(makeAllStrings(bascHashi))

#print(makeAllStrings(solveHashi(fourCase)))

#print(makeAllStrings([[0, 2, 0, 0, 1, 0, 1, 0], [1, 0, 0, 2, 'c', 3, 0, 2], [0, 2, 0, 0, 'c', 0, 0, 0], [2, 0, 0, 0, 'c', 2, 0, 3], ['c', 2, 0, 3, 'c', 0, 1, 0], [1, 'd', 0, 'c', 'x', 0, 'c', 'x'], ['c', 'x', 'b', 'x', 'a', 'a', 'x', 'd'], ['x', 'b', 'x', 'a', 1, 'a', 'a', 'x']]))
#print(checkObstruction(bridHashi,4,2,4,6))

taa = [[0, 2, 0, 0, 2, 0, 1, 0],
       [1, 0, 0, 2, 0, 3, 0, 2], 
       [0, 2, 0, 0, 0, 0, 0, 0], 
       [2, 0, 0, 0, 0, 2, 0, 3], 
       ['c', 2, 0, 4, 0, 0, 2, 0], 
       [1, 'd', 0, 0, 1, 0, 0, 2], 
       ['c', 'x', 'b', 1, 0, 0, 1, 0], 
       [1, 0, 2, 0, 3, 0, 0, 2]]

taa3 = [[0, 2, 0, 0, 2, 0, 1, 0],
       [1, 0, 0, 2, 0, 3, 0, 2], 
       [0, 2, 0, 0, 0, 0, 0, 0], 
       [2, 0, 0, 0, 0, 2, 0, 3], 
       ['c', 'x', 0, 4, 0, 0, 2, 0], 
       [1, 'd', 0, 0, 1, 0, 0, 2], 
       ['c', 'x', 'b', 'x', 0, 0, 1, 0], 
       [1, 0, 2, 0, 3, 0, 0, 2]]

taa2 = [[0, 2, 0, 0, 2, 0, 1, 0],
       [1, 0, 0, 2, 0, 3, 0, 2], 
       [0, 2, 0, 0, 0, 'c', 0, 0], 
       [2, 0, 0, 0, 0, 'c', 0, 3], 
       ['c', 2, 'a', 'a', 'a', 4, 2, 0], 
       [1, 'd', 0, 0, 1, 0, 0, 2], 
       ['c', 'x', 'b', 1, 'a', 'a', 1, 0], 
       [1, 0, 2, 0, 3, 0, 0, 2]]

taa4 = [[0, 2, 0, 0, 2, 0, 1, 0],
        [1, 0, 0, 2, 0, 3, 0, 2], 
        [0, 2, 0, 0, 0, 0, 0, 0], 
        [3, 0, 0, 0, 0, 2, 0, 3], 
        [0, 2, 0, 4, 0, 0, 2, 0], 
        [3, 'd', 0, 0, 1, 0, 0, 2], 
        [0, 'x', 'b', 1, 0, 0, 1, 0], 
        [2, 0, 2, 0, 3, 0, 0, 2]]

#print(originalValue(taa4,6,3))

#print(findConnectedComponent(taa3,4,1))
#print(isComponentIsolated(taa3,4,1))
#print(sameRowX(taa,6,1))
#print(hasBridge(taa,4,1,6,1))

#print(allAdjacent(taa,7,2))
#print(taa[7][2])

###print(findConnectedComponent([['x', 'a', 'a', 'x', 'a', 'x', 0], ['c', 0, 'x', 'a', 1, 0, 'x'], ['x', 'a', 'a', 'x', 'c', 0, 'c'], ['c', 0, 0, 'c', 1, 0, 'x'], ['c', 'x', 'a', 'x', 'c', 0, 'c'], ['x', 'a', 'x', 'a', 'x', 0, 'c'], [0, 'x', 'a', 'x', 'b', 'b', 'x']],1,6))

#print(allPossibleBridges([[0, 2, 0, 0, 2, 0, 1, 0], [1, 0, 0, 2, 0, 3, 0, 2], [0, 2, 0, 0, 0, 0, 0, 0], [2, 0, 0, 0, 0, 2, 0, 3], ['c', 2, 0, 4, 0, 0, 2, 0], [1, 'd', 0, 0, 1, 0, 0, 2], ['c', 'x', 'b', 1, 0, 0, 1, 0], [1, 0, 1, 'a', 1, 'a', 'a', 1]]))